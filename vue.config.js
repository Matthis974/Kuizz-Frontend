const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
var JavaScriptObfuscator = require('webpack-obfuscator');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    productionSourceMap: false,
    pwa: {
      workboxPluginMode: 'InjectManifest',
      workboxOptions: {
        swSrc: './src/serviceWorker.js',
        swDest: 'service-worker.js',
      },
    },
    configureWebpack: {
      
      plugins: [
        
        new VuetifyLoaderPlugin(),
        process.env.NODE_ENV === 'production' ? new JavaScriptObfuscator({ rotateUnicodeArray: true, selfDefending: true, disableConsoleOutput: true }) : () => {},
        process.env.NODE_ENV === 'profile' ? new BundleAnalyzerPlugin() : () => {},
        new HtmlWebpackPlugin(
          {
            title: 'Kuizz',
            page_title: 'KuiZZ',
            long_title: 'KuiZZ',
            description: 'KuiZZ is a quizz website that aims to create a global with it\'s real time quizz system! Fight friends or work together!',
            template: 'public/index.html',
            BASE_URL: '/'
          }
        )
      ]
    }
}
