self.addEventListener('message', (event) => {
    if (!event.data)
        return
    switch (event.data) {
        case 'skipWaiting':
            self.skipWaiting()
            break
        default:
            // No-operation
            break
    }
})
self.addEventListener('push', function(event) {
    const payload = event.data ? event.data.json() : {};
    if(payload === {}) return
    if(payload.livenessCheck)
        return
    event.waitUntil(
        self.registration.showNotification(payload.title, {
            ...payload,
            title: undefined,
        })
    );
})
console.log('Hello from the service worker.')
workbox.core.clientsClaim()
self.__precacheManifest = [].concat(self.__precacheManifest || [])
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})
