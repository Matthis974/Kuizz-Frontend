// You still need to register Vuetify itself
// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import Vuetify_ from 'vuetify'
import { Store } from 'vuex'
import { AppState } from '@/core/store/extensions/version/Store'

Vue.use(Vuetify)

export default (store: Store<AppState>) => {
    const vuetify = new Vuetify({
        theme: {
            dark: store.state.Customization.darkTheme,
            options: {
                themeCache: {
                    get: key => localStorage.getItem(key.toString()),
                    set: (key, value) => localStorage.setItem(key.toString(), value),
                }
            }
        },
        icons: {
            iconfont: 'mdiSvg',
        },
    }) as typeof Vuetify_
    return vuetify
}
