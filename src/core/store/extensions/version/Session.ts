import User from '@/core/api/typings/User'
import { Module, ActionContext } from 'vuex'
import { AppState } from './Store'
import { openLoginPopup } from '../utils/popup'
import { getProfile } from '@/core/api/User'
import { InAppNotification } from './Notifications'

export class SessionState {
    public user?: User | null = null
    public token?: string | null = null
    public loggedIn: boolean = false
}

export default class SessionStore implements Module<SessionState, AppState> {
    public namespaced = true
    public mutations = {
        beforeLogin(state: SessionState) {
            state.loggedIn = false
            state.token = null
            state.user = null
        },
        tokenAcquired(state: SessionState, token: string) {
            state.token = token
        },
        profileLoaded(state: SessionState, profile: any) {
            state.user = profile
            state.loggedIn = true
        },
        loginFailed(state: SessionState, error: any) {
            state.loggedIn = false
            state.token = null
            state.user = null
        },
    }
    public actions = {
        login({ commit, dispatch }: ActionContext<SessionState, AppState>, provider: string) {
            commit('beforeLogin')
            openLoginPopup(provider)
                .then(async (tokenPayload) => {
                    commit('tokenAcquired', tokenPayload.token)
                    const profile = await getProfile()
                    commit('profileLoaded', profile)
                    dispatch('Notifications/push', {
                        timeout: 5000,
                        message: `Welcome ${profile.username}!`,
                        color: 'green',
                    } as InAppNotification, { root: true })
                })
                .catch((error) => {
                    commit('loginFailed', error)
                    dispatch('Notifications/push', {
                        timeout: 5000,
                        message: `Login failed : ${error}`,
                        color: 'red',
                    } as InAppNotification, { root: true })
                })
        },
        logout({ commit, dispatch }: ActionContext<SessionState, AppState>) {
            commit('beforeLogin')
            dispatch('Notifications/push', {
                timeout: 5000,
                message: `Logged out`,
                color: 'red',
            } as InAppNotification, { root: true })
        },
        async reloadProfile({ commit, dispatch }: ActionContext<SessionState, AppState>) {
            const profile = await getProfile()
            commit('profileLoaded', profile)
            dispatch('Notifications/push', {
                timeout: 5000,
                message: 'Profile reloaded',
                color: 'red',
            } as InAppNotification, { root: true })
        },
        async link({ commit, dispatch, state }: ActionContext<SessionState, AppState>, provider: string) {
            await openLoginPopup(provider, state.token)
        },
    }
    public state = () => new SessionState()
}
