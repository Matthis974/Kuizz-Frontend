import { Module, ActionContext } from 'vuex'
import { AppState } from './Store'

export class NotificationsState {
    public notifications: InAppNotification[] = []
}

export type InAppNotification = {
    timeout: number,
    message: string,
    color: string,
}


export default class NotificationsStore implements Module<NotificationsState, AppState> {
    public namespaced = true
    public actions = {
        shift({ commit }: ActionContext<NotificationsState, AppState>) {
            commit('shift')
        },
        push({ commit }: ActionContext<NotificationsState, AppState>, notification: InAppNotification) {
            commit('push', notification)
        },
    }
    public mutations = {
        shift(store: NotificationsState) {
            store.notifications.shift()
        },
        push(store: NotificationsState, notification: InAppNotification) {
            store.notifications.push(notification)
        },
    }
    public state = () => new NotificationsState()
}
