import { Module, ActionContext } from 'vuex'
import User from '@/core/api/typings/User'
import { getProfile } from '../../api/User'

type UsersState = {
    user: User[],
}

class UsersModule implements Module<UsersState, {}> {
    public namespaced = true

    public mutations = {
        request(state: UsersState, id: string) {
            state.user.push({
                id,
                loaded: false,
            } as any)
        },
        success(state: UsersState, user: User) {
            for (const i in state.user) {
                if (state.user[i].id === user.id) {
                    state.user[i] = user;
                    break
                }
            }
        },
        failed(state: UsersState, { user, error }) {
            for (const i in state.user) {
                if (state.user[i].id === user) {
                   (state.user[i]as any).error = error
                }
            }
        },
    }

    public actions = {
        async load({ commit, dispatch }: ActionContext<UsersState, {}>, user: string) {
            commit('request', user)
            await getProfile(user)
                .then((userProfile) => {
                    commit('success', userProfile)
                }, (error) => {
                    commit('failed', { user, error })
                })
        },
    }

    public getters = {
        user(state: UsersState) {
            return state.user
        },
    }

    public state() {
        return {
            user: [],
        }
    }

}

export default new UsersModule()
