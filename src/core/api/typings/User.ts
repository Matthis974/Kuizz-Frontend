type GenericLinkedAccount = {
    avatar: string,
    id: string,
}

type User = {
    id: string,
    discord?: GenericLinkedAccount,
    google?: GenericLinkedAccount,
    reputation: number,
    roles: number,
    wins: number,
    lost: number,
    username: string,
    biography: string,
    avatar: string,
    avatarSource: 0x1 | 0x2,
    locale: string,
}
export default User
