import { API_ENDPOINT } from '.'
import User from './typings/User'
import Axios from 'axios'

export function getLeaderboard(): Promise<User[]> {
    return Axios(`${API_ENDPOINT}/api/users/leaderboard`)
        .then((x) => x.data)
}
